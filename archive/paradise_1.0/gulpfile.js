//  requires

const gulp = require('gulp');
const sass = require('gulp-sass');
const webserver = require('gulp-webserver');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const autoprefixer = require('gulp-autoprefixer');
const tinypng = require('gulp-tinypng-compress');
const modernizr = require('gulp-modernizr');
const cssbeautify = require('gulp-cssbeautify');
const fileinclude = require('gulp-file-include');
const htmlbeautify = require('gulp-html-beautify');

//  sass and vendor prefixes

const autoprefixerOptions = {
    browsers: [
        'last 2 version',
        'safari 5',
        'ie 7', 'ie 8', 'ie 9',
        'opera 12.1',
        'ios 6', 'android 4'
    ]
};

gulp.task('styles', function() {
    gulp.src('css/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(cssbeautify())
        .pipe(gulp.dest('./css/'))
});

//  modernizr

gulp.task('modernizr', function() {
    gulp.src('./scripts/modules/*.js')
    .pipe(modernizr({
        options: [
            "setClasses",
    		"addTest",
    		"html5printshiv",
    		"testProp",
    		"fnBind"
        ]
    }))
    .pipe(gulp.dest("./scripts/modules/"))
});

//  html beautify

gulp.task('htmlbeautify', function() {
    var options = {
        indentSize: 4,
        preserve_newlines: true
    };
    gulp.src('dist/*.html')
        .pipe(htmlbeautify(options))
        .pipe(gulp.dest('dist/'))
});

//  file include

gulp.task('fileinclude', function() {
    gulp.src(['*.html'])
        .pipe(fileinclude({
            prefix: '@',
            basepath: '@root',
            type: JSON
        }))
        .pipe(gulp.dest('dist'));
});

//  minify js files

gulp.task('compress', function() {
    gulp.src('./scripts/modules/*.js')
    .pipe(minify({
        ext:{
            min:'-min.js'
        }
    }))
    .pipe(gulp.dest('dist/scripts'))
});

//  webserver - launch localhost

gulp.task('webserver', function() {
    gulp.src('dist/')
    .pipe(webserver({
        livereload: true,
        directoryListing: false,
        open: false,
        port: 9000
    }));
});

//  copy files

gulp.task('copyHtml', function() {
    gulp.src('*.{,html}')
        .pipe(gulp.dest('dist/'));
});

gulp.task('copyCss', function() {
    gulp.src('css/*.{,css}')
        .pipe(gulp.dest('dist/css/'));
});

//  run tasks + watch

gulp.task('prod', ['htmlbeautify']);
gulp.task('default', ['webserver', 'watch', 'compress', 'fileinclude', 'copyCss']);
gulp.task('watch', function() {
    gulp.watch('./scripts/modules/*.js', ['compress']);
    gulp.watch('css/sass/*.scss',['styles']);
    gulp.watch('partials/*.html', ['fileinclude']);
    gulp.watch('css/sass/*.scss', ['copyCss']);
})
