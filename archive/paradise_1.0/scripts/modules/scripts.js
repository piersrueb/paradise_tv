//  js document

let source = document.getElementById('player');
let v = document.getElementById('video');
let mute = document.getElementById('mute');
let holder = document.getElementById('holder');
let prog = document.getElementById('progress');
let fs = document.getElementById('fullscreen');
let letters = document.getElementsByClassName('letter');
let d;
let utc;
let hPer;
let wPer;
let utcSec;
let deadLineSec;
let loadTime;
let ww = window.innerWidth;
let wh = window.innerHeight;
let colours = ['#39ff14', '#ccff00', '#ff8300', '#ff69b4', '#4d4dff'];
let randColour = Math.floor((Math.random() * 4) + 0);
let videoMuted = true;
let vTimer = setInterval(function(){ timeCode() }, 500);
let videoLoaded = false;
let cv = 0;
let title = [
    'Intermission',
    'Ways Of Seeing',
    'Intermission',
    'Link Wray',
    'Intermission',
    'Shadows',
    'Intermission',
    'Queens At Heart',
    'Intermission',
    'Blade Runner'
];
let film = [
    '001_INT_EANDC_000300_050319.mp4',
    '002_PRG_WaysOfSeeing_002952_050319.mp4',
    '003_INT_EANDC_000507_050319.mp4',
    '004_PRG_LinkWray_005958_050319.mp4',
    '005_INT_EANDC_000501_050319.mp4',
    '006_PRG_Shadows_012159_050319.mp4',
    '007_INT_EANDC_000300_050319.mp4',
    '008_PRG_QueensAtHeart_002138_050319.mp4',
    '009_INT_EANDC_000320_050319.mp4',
    '010_PRG_BladeRunner_004743_050319.mp4'
];
let duration = [
    180000,   // 1
    1792000,  // 2
    308000,   // 3
    3598000,  // 4
    302000,   // 5
    4920000,  // 6
    180000,   // 7
    1299000,  // 8
    200000,   // 9
    2863000   // 10
];
let dl = 1551800017000;  //  start time Timestamp in milliseconds
let deadline = [dl];
let films = film.length;
let endTime;
let x = 0;

//  milisecond converter

const msConvert = (h, m, s) => {
    let a = h * 60;
    let b = a * 60;
    let c = m * 60;
    let d = b + c + s;
    let ms = d * 1000;
    console.log('ms: ' + ms);
}
msConvert(0, 47, 43);

//  build the timings array

duration.forEach(elem => {
    x = x + elem;
    deadline.push(dl + x);
});

//  colours

document.body.style.backgroundColor = colours[randColour];
v.style.backgroundColor = colours[randColour];
for (let i = 0; i < letters.length; i++) {
    letters[i].style.fill = colours[randColour];
}

//  top margin

const topMar = () => {
    wh = window.innerHeight;
    if(wh > 540){
        let w = v.getAttribute('height');
        let x = parseInt(w);
        let y = wh - x;
        let z = y / 2;
        holder.style.marginTop = z + 'px';
    }
}

//  size the video maintaining aspect ratio

const videoSize = () => {
    ww = window.innerWidth;
    if(ww < 1280){
        wPer = ww / 100;
        hPer = wPer * 56.25;
        v.setAttribute('width', ww + 'px');
        v.setAttribute('height', hPer + 'px');
    } else{
        v.setAttribute('width', '1280px');
        v.setAttribute('height', '720px');
    }
    topMar();
}
videoSize();
window.addEventListener('resize', videoSize);

//  video stuff

const theTime = () => {
    d = new Date(),
    utc = d.getTime();  //  timestamp
}

//  timecode

const timeCode = () => {
    theTime();
    if(videoLoaded === false){
        endTime = deadline[cv] + duration[cv];
        utcSec = utc / 1000;
        deadLineSec = deadline[cv] / 1000;
        loadTime = utcSec - deadLineSec;
        if(utc > deadline[cv] && utc < endTime){
            mute.classList.add('mute-active');
            fs.classList.add('fs-active');
            v.style.opacity = '1';
            source.src = 'video/' + film[cv];
            v.load();
            v.play();
            v.currentTime = loadTime;  //  set the playhead
            videoLoaded = true;
            document.title = title[cv] + ' - Paradise TV';
            console.log(title[cv] + ' - Paradise TV');
            //  safari fix
            v.addEventListener('loadedmetadata', metaLoaded);
            function metaLoaded(){
                v.currentTime = loadTime;
                setTimeout(function(){
                    v.currentTime = loadTime;
                }, 1000);
            }
        }
    }  //  end event
    if(utc > endTime) {
        videoLoaded = false;
        v.pause();
        source.src = '';
        v.style.opacity = '0';
        if(cv < films - 1){
            cv++;
        } else{
            clearInterval(vTimer);
            mute.classList.remove('mute-active');
            document.title = 'Paradise TV';
            console.log('The end');
        }
    }
    progBar();
}

//  mute button

const muteControl = () => {
    if(videoMuted === true){
        v.muted = false;
        videoMuted = false;
    } else{
        v.muted = true;
        videoMuted = true;
    }
}
mute.addEventListener('click', muteControl);

//  fullscreen button

const fullScreen = () => {
    if (v.requestFullscreen) {
        v.requestFullscreen();
    } else if (v.mozRequestFullScreen) {  //  Firefox
        v.mozRequestFullScreen();
    } else if (v.webkitRequestFullscreen) { //  Chrome, Safari and Opera
        v.webkitRequestFullscreen();
    } else if (v.msRequestFullscreen) { //  IE/Edge
        v.msRequestFullscreen();
    }
}
fs.addEventListener('click', fullScreen);

//  full screen test

let isFs = false;

const fsTest = () => {
    if(isFs === false){
        v.classList.add('black');
        isFs = true;
    } else {
        v.classList.remove('black');
        isFs = false;
    }
}
window.addEventListener('fullscreenchange', fsTest);

//  progress bar

const progBar = () => {
    let cur = v.currentTime;
    let dur = v.duration;
    let dp = dur / 100;
    let cp = cur / dp;
    prog.style.width = cp + '%';
}

const convertTime = (totalSeconds) => {
    let hours   = Math.floor(totalSeconds / 3600);
    let minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
    let seconds = totalSeconds - (hours * 3600) - (minutes * 60);
    seconds = Math.round(seconds * 100) / 100
    let result = (hours < 10 ? '0' + hours : hours);
    result += '.' + (minutes < 10 ? '0' + minutes : minutes);
    result += '.' + (seconds  < 10 ? '0' + seconds : seconds);
    return result;
}

//  miliseconds back to date

const dateConvert = (time) =>{
    let x = time;
    let y = new Date(x);
    let z = y.toLocaleString();
    console.log('Start time: ' + z);
}
dateConvert(dl);
