//  requires

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const autoprefixer = require('gulp-autoprefixer');
const tinypng = require('gulp-tinypng-compress');
const cssbeautify = require('gulp-cssbeautify');
const fileinclude = require('gulp-file-include');
const connect = require('gulp-connect-php');
const browserSync = require('browser-sync');

//  sass and vendor prefixes

const autoprefixerOptions = {
    browsers: [
        'last 2 version',
        'safari 5',
        'ie 7', 'ie 8', 'ie 9',
        'opera 12.1',
        'ios 6', 'android 4'
    ]
};

gulp.task('styles', function() {
    gulp.src('css/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(cssbeautify())
        .pipe(gulp.dest('./css/'))
});

//  file include

gulp.task('build', function() {
    gulp.src(['*.php'])
        .pipe(fileinclude({
            prefix: '@',
            basepath: '@root',
            type: JSON
        }))
        .pipe(gulp.dest('dist'));
});

//  minify js files

gulp.task('compress', function() {
    gulp.src('./scripts/modules/*.js')
    .pipe(minify({
        ext:{
            min:'-min.js'
        }
    }))
    .pipe(gulp.dest('dist/scripts'))
});

//  webserver

gulp.task('connect-sync', function() {
    connect.server({}, function (){
        browserSync({
            proxy: '127.0.0.1:8000/dist/'
        });
    });
    gulp.watch('dist/*.php').on('change', function () {
        browserSync.reload();
    });
});

//  copy files

gulp.task('copyCss', function() {
    gulp.src('css/*.{,css}')
        .pipe(gulp.dest('dist/css/'));
});

//  run tasks + watch

gulp.task('default', ['watch', 'build', 'compress', 'copyCss', 'connect-sync']);
gulp.task('watch', function() {
    gulp.watch('partials/*.html',['build']);
    gulp.watch('*.php',['build']);
    gulp.watch('./scripts/modules/*.js', ['compress']);
    gulp.watch('css/sass/*.scss',['styles']);
    gulp.watch('css/sass/*.scss', ['copyCss']);
})
