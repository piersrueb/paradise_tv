# Paradise TV

Set the start time within the js file to schedule your video. It uses UNIX time, you can convert it from the standard format on the link below.

https://www.epochconverter.com/

https://www.epochconverter.com/days/2019

```

   |\          .(' *) ' .
   | \       '.*) paradise )*
   |(*\      .*(// .*) .
   |___\       // (. '*
   ((("'\     // '  * .
   ((c'7')   /\)
   ((((^))  /  \
 .-')))(((-'   /
    (((()) __/'
     )))( |
      (()
       ))

```

Compile your SASS:

```
npm run watch-css
```
With a watch task:

```
npm run build-css
```
Run PHP locally:

```
php -S localhost:8000
```
