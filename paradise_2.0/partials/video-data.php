<?php
    $dir    = 'video';
    $files = scandir($dir);
    $exc = [
        '.',
        '..',
        '.DS_Store',
        'schedule.js'
    ];
    foreach ($files as $value) {
        if(!in_array($value,$exc)) {
            echo "<div class='data-holder' data-video='$value'></div>";
        }
    }
?>
