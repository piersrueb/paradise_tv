<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>Paradise TV</title>
        <link rel="shortcut icon" href="img/icon.png" />
        <link rel="icon" type="image/png" href="img/icon.png" />
        <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    	<link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body id="ur-<?php date_default_timezone_set('UTC'); echo date('z'); ?>" data-day="<?php date_default_timezone_set('UTC'); echo date('z'); ?>">
        <main>
            <section id="holder">
                <?php
                    include 'partials/video.php';
                    include 'partials/controls.php';
                ?>
            </section>
        </main>
        <?php
            include 'partials/video-data.php';
            include 'partials/tracking.php';
        ?>
    </body>
</html>
